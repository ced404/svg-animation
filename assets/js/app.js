
// Network Animation
let networkAnimation = (function (TweenMax) {
	"use strict";

	const State = {
		element: null,
		animations: {
			bounce: function (object) {
				TweenMax.to (object, 0.15, {scale:.5, ease: Expo.easeOut});
				TweenMax.to (object, 0.2, {scale:1, delay:0.15});
			},
		},
	};


	const play = function () {
		requestAnimationFrame (() => {
			var tween = new TimelineMax()
			// gateway
			.add (TweenMax.to (State.gateway, .25, {autoAlpha: 1}))

			// connections
			.add (TweenMax.to (State.connections, .25, {autoAlpha: 1, transform: 'translateY(0)', ease: Expo.easeOut}))

			// servers
			.add (TweenMax.to (State.server1, .25, {autoAlpha: 1}))
			.add (TweenMax.to (State.server2, .25, {autoAlpha: 2}))
			.add (TweenMax.to (State.server3, .25, {autoAlpha: 3}))
		});
	};


	const init = function () {
		// SVG element
		State.element = document.querySelector ('[data-animation="network"] svg');

		// SVG paths
		State.connections = State.element.querySelector ('#connections');
		State.network = State.element.querySelector ('#network');
		State.gateway = State.element.querySelector ('#gateway');
		State.server1 = State.element.querySelector ('#server1');
		State.server2 = State.element.querySelector ('#server2');
		State.server3 = State.element.querySelector ('#server3');

		// Initial states
		TweenMax.to (State.connections, 0, {autoAlpha: 0, transform: 'translateY(-240px)'})
		TweenMax.to (State.gateway, 0, {autoAlpha: 0})
		TweenMax.to (State.server1, 0, {autoAlpha: 0})
		TweenMax.to (State.server2, 0, {autoAlpha: 0})
		TweenMax.to (State.server3, 0, {autoAlpha: 0})
	};


	return {
		"init": init,
		"play": play,
	};

})(TweenMax);


networkAnimation.init ();
networkAnimation.play ();
